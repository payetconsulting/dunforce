<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    /**
     * BookRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Book::class);
    }

    /**
     * @param array $filter
     * @return mixed
     */
    public function getBookUsingFilter(Array $filter)
    {
        $builder = $this->createQueryBuilder('b');

        if(!empty($filter['beforeDate']))
        {
            $builder
                ->andWhere('b.creationDate <= :beforeDate')
                ->setParameter(':beforeDate', $filter['beforeDate']);
        }

        if(!empty($filter['afterDate']))
        {
            $builder
                ->andWhere('b.creationDate >= :afterDate')
                ->setParameter(':afterDate', $filter['afterDate']);
        }

        if(!empty($filter['nationality']))
        {
            $builder
                ->innerJoin(Author::class, 'a')
                ->andWhere('a.id = b.author')
                ->andWhere('a.nationality = :nationality')
                ->setParameter('nationality', $filter['nationality']);
        }

        return $builder->getQuery()->getResult();
    }
}
