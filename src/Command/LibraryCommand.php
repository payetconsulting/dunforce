<?php

namespace App\Command;

use App\Repository\BookRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class LibraryCommand
 * @package App\Command
 */
class LibraryCommand extends Command
{
    /**
     * configuration method
     */
    protected function configure()
    {
        $this->setName('app:mylibrary')
            ->setDescription('Show Authors and/or Books')
            ->setHelp('This command allows you to see authors and/or books saved on database.')
            ->addArgument('type', InputArgument::OPTIONAL, 'Type of data : all, authors, books', 'all');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(['MyLibrary Command', '**************','']);

        switch($input->getOption('type'))
        {
            case "all":
                $this->searchAllData($output);
                break;
            case "authors":
                $this->searchAuthorsData($output);
                break;
            case "books":
                $this->searchBooksData($output);
                break;
            default:
                throw new Exception('Type not allowed !');
        }
    }

    /*
     *
     */
    private function searchAllData(OutputInterface $output) : string
    {
        $output->writeln('Type : all (books + authors)');
    }

    private function searchAuthorsData(OutputInterface $output) : string
    {
        $output->writeln('Type : authors only');


    }

    private function searchBooksData(OutputInterface $output) : string
    {
        $output->writeln('Type : books only');

        

    }
}