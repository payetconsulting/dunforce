<?php

namespace App\Controller;

use App\Entity\Book;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiController
 * @package App\Controller
 */
class ApiController extends AbstractController
{
    /**
     * @param Request $request
     * @return static
     */
    public function books(Request $request)
    {
        $data = $this->getDoctrine()->getRepository(Book::class)->getBookUsingFilter([
            'beforeDate'  => $request->get('beforeDate'), 
            'afterDate'   => $request->get('afterDate'), 
            'nationality' => $request->get('nationality')
        ]);

        $responseData = [
            'books' => array_map(function(Book $book) {
                $author = $book->getAuthor();
                return [
                    'id'    => $book->getId(),
                    'title' => $book->getTitle(),
                    'cover_picture' => $book->getCoverPicture(),
                    'isbn'   => $book->getIsbn(),
                    'creationDate' => $book->getCreationDate()->format('Y-m-d H:i:s'),
                    'tags'   => $book->getTagsArray(),
                    'author' => [
                        'id' => $author->getId(),
                        'firstname' => $author->getFirstName(),
                        'lastname' => $author->getLastName(),
                        'nationality' => $author->getNationality(),
                    ]
                ];
            },$data)
        ];

        return JsonResponse::create($responseData, 200);
    }
}