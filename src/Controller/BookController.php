<?php

namespace App\Controller;

use App\Entity\Author;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Book;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BookController
 * @package App\Controller
 */
class BookController extends AbstractController {

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $id = (int)$request->get('id');

        /** @var Book $book */
        $book = $this->getDoctrine()->getRepository(Book::class)->find($id);

        if(!$book) {
            return $this->redirectToRoute('home');
        }

        $form = $this->getForm($book);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($data);
            $entityManager->flush();
        }

        return $this->render('page/book/edit.html.twig', [
            'bookId' => $id,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $form = $this->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();


            $book = new Book();
            $book
                ->setAuthor($data['author'])
                ->setCoverPicture($data['coverPicture'])
                ->setCreationDate(new \DateTime('now'))
                ->setIsbn($data['isbn'])
                ->setTags($data['tags'])
                ->setTitle($data['title']);
            

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('page/book/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param null $book
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getForm($book = null)
    {
        $builder = $this->createFormBuilder($book);

        $form = $builder
            ->add('title', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add($builder->create('author', EntityType::class, [
                'class' => Author::class,
                'choice_label' => 'Author',
                'expanded' => false,
                'attr' => [
                    'class' => 'form-control'
                ]
            ]))
            ->add('coverPicture', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('isbn', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('tags', TextType::class, [
                'label' => 'tags (eq: "friend, surprise")',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
            ->getForm();

        return $form;
    }
}