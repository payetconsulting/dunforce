<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Author;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AuthorController
 * @package App\Controller
 */
class AuthorController extends AbstractController {

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $id = (int)$request->get('id');

        /** @var Author $author */
        $author = $this->getDoctrine()->getRepository(Author::class)->find($id);

        if(!$author) {
            return $this->redirectToRoute('home');
        }

        $form = $this->getForm($author);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($data);
            $entityManager->flush();
        }

        return $this->render('page/author/edit.html.twig', [
            'authorId' => $id,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        $form = $this->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();

            $author = new Author();
            $author
                ->setFirstName($data['first_name'])
                ->setLastName($data['last_name'])
                ->setNationality($data['nationality']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($author);
            $entityManager->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('page/author/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param null $author
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getForm($author = null)
    {
        $form = $this->createFormBuilder($author)
            ->add('first_name', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('last_name', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('nationality', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
            ->getForm();

        return $form;
    }

}