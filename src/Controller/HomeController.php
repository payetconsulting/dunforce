<?php

namespace App\Controller;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends AbstractController {

    const PASSWORD_SALT = "dunforce_passwd_salt";

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $authors = $this->getDoctrine()->getRepository(Author::class)->findAll();
        $books   = $this->getDoctrine()->getRepository(Book::class)->findAll();

        if($this->getUser()) {
            
        }

        return $this->render('page/home.html.twig', [
            'authors' => $authors,
            'books'   => $books,
            'user'    => $this->getUser()
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function signup(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('username', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control'
                ],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'required' => true,
                'first_options'  => [
                    'label' => 'Password'
                ],
                'second_options' => [
                    'label' => 'Repeat Password'
                ],
                'options' => [
                    'attr' => ['class' => 'form-control']
                ]
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
            ->getForm();

        $form->handleRequest($request);

        $errorString = '';

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if(empty($data['username']))
            {
                $errorString = 'Username cannot be empty !';
            }
            elseif(empty($data['password']))
            {
                $errorString = 'Password cannot be empty !';
            }
            else
            {
                return $this->saveUser($data);
            }
        }

        return $this->render('page/signup.html.twig', [
            'form'        => $form->createView(),
            'errorString' => $errorString
        ]);
    }

    /**
     * @param array $userData
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function saveUser(Array $userData)
    {
        $user = new User();
        $user
            ->setUsername($userData['username'])
            ->setPassword(crypt($userData['password'], self::PASSWORD_SALT))
            ->setApiKey(md5(uniqid()));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute('home');
    }

}