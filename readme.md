# Installation

First, please specify your database settings on your .env file.

(do not create your database manually)

<code>
DATABASE_URL=mysql://{user}:{password}@127.0.0.1:{port}/{database}
</code>


Replace variables by your settings :

* {user}
* {password}
* {port}
* {database}

To create your database :

<code>
php bin/console doctrine:database:create
</code>

Generate your migration file from all entities :

<code>
php bin/console make:migration
</code>

Execute your migration file with this command line :

<code>
php bin/console doctrine:migrations:migrate
</code>

If you change your table settings (new column for example), you can generate your migration file with this command line :

<code>
php bin/console make:migration
</code>

And execute your migration file with this command :

<code>
php bin/console doctrine:migrations:migrate
</code>


# Start your website

<code>
php bin/console server:start
</code>