<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use App\Controller\HomeController;
use App\Controller\AuthorController;
use App\Controller\BookController;
use App\Controller\ApiController;

$routes = new RouteCollection();

$routes->add('home', new Route('/', array(
    '_controller' => [HomeController::class, 'index']
)));

$routes->add('author_create', new Route('/author/create', array(
    '_controller' => [AuthorController::class, 'create']
)));

$routes->add('author_edit', new Route('/author/{id}', array(
    '_controller' => [AuthorController::class, 'edit']
)));

$routes->add('book_create', new Route('/book/create', array(
    '_controller' => [BookController::class, 'create']
)));

$routes->add('book_edit', new Route('/book/{id}', array(
    '_controller' => [BookController::class, 'edit']
)));

$routes->add('books', new Route('/api/books', array(
    '_controller' => [ApiController::class, 'books']
)));

$routes->add('signup', new Route('/signup', array(
    '_controller' => [HomeController::class, 'signup']
)));

$routes->add('logout', new Route('/logout', []));

return $routes;